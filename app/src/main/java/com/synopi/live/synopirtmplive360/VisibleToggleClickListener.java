package com.synopi.live.synopirtmplive360;
/*
// created by uttam kumar
// for handling setting view visibility management
 */

import android.view.View;

public abstract class VisibleToggleClickListener implements View.OnClickListener {

    private boolean mVisible;

    @Override
    public void onClick(View v) {
        mVisible = !mVisible;
        changeVisibility(mVisible);
    }

    protected abstract void changeVisibility( boolean visible);

}
